# ⚙️📈 Stats
Stats is a [Pip-Bot](https://gitlab.com/Polyfox/Pip-Bot)-plugin that displays
stats about the bot.

# Licenses
- Code: licensed under the MIT license.
- GitLab project avatar: [Icon](https://www.flaticon.com/free-icon/analytics_315384) by [Freepik](https://www.freepik.com/) from [Flaticon](https://www.flaticon.com), licensed CC 3.0 BY